package Data::Interactive::Walker;

use 5.010;
use strict;
use warnings;

use Carp;
use Term::ReadLine;

	use Data::Dump;

our $VERSION = '0.01';

# perl -I ./lib -MData::Interactive::Walker -e "$w=Data::Interactive::Walker->new; $w->{commands}{dumper}->([1,2,{a=>42}])"

#perl -MData::Dumper::Compact=ddc -le'print ddc sub { "Foo!" }' # Can be handy for inspection purposes.

BEGIN {
    # force Term::ReadLine to load the Term::ReadLine::Perl if present
    $ENV{PERL_RL} = "Perl";
    # TAB completion made possible on win32 via Term::Readline with TERM=
    $ENV{TERM}    = 'not dumb' if $^O eq 'MSWin32';
}


sub new{
	my $class = shift;
	
	my %opts = _check_opts( @_ );
	
	$opts{rl} = Term::ReadLine->new('');
	$opts{data} //= [1,2,3,{a=>42, b=>[4,5,6,7=>{c=>333}]}];
	
	$opts{container_lbl} = '/';
	$opts{currentlevel} = [];
	$opts{current} = undef;
	$opts{ancestors} = ();
	#$opts{commands} = undef;
	
	
	return bless { %opts }, $class;
}

sub walk{
	my $self = shift;

	$self->{current} = $self->{data};
	push @{$self->{ancestors}}, $self->{container_lbl};

	# lines below is the first time initalization for autocompletion
	$self->{rl}->Attribs->{completion_function} = sub {
		my $txt = shift;
		return grep { /^$txt/i } grep $_ !~ /^[\.\+]{1,2}$/, (keys %{ $self->{commands} }, @{ $self->{currentlevel} } );
	};
	
	# open 
	
	$self->open_element();
	
	# interactive part of the program
	while ( defined( my $input = $self->{rl}->readline( '>' ) ) ) {
		chomp $input;
		# readline appends a space
		$input = ($input =~ s/\s+$//r);
		# commands
		if ( $self->{commands}->{$input} ){
			#$self->{commands}->{$input}->( $self->{data} );
			$self->{commands}->{$input}->( $self->{current} );
		}
		# elements
		else{
			 print "actually current ref: ",(ref $self->{current}),"\n", dd $self->{current};
			if ( 'ARRAY' eq ref $self->{current} ){
				$self->{current} = $self->{current}->[ $input ];
				$self->open_element();
			}
			
			
		}
		
		
		# else{
			# print "current ref: ",(ref $self->{current}),"\n", dd $self->{current};
			
			# if ( exists $self->{current}->[$input] ){
				# $self->{current} = $self->{current}->[$input];
				# $self->open_element();
			# }
			# elsif( exists $self->{current}->{$input} ){
				# $self->{current} = $self->{current}->{$input};
				# $self->open_element();
			# }
			# else{ print "plain scalar: [$input]\n" }
		# }

	}
	
}
# <Discipulus> can someone explain me the Warning on exists $array[$i] https://perldoc.perl.org/functions/exists ?
# <LeoNerd> How do you mean "explain"? The warning is that you used it on an array element. You shouldn't
# <hobbs> there is a difference internally between an element of an array being !exists and !defined, but it's not one that you can easily do anything useful with
# <hobbs> (see also perldoc -f delete)
# <Discipulus> the doc point to 'delete' and this is easy to understand
# <Discipulus> WARNING: Calling exists on array values is strongly discouraged. The notion of deleting or checking the existence of Perl array elements is not conceptually coherent, and can lead to surprising behavior.
# <hobbs> I've never agreed that it's all that confusing or "incoherent" in the years since that note was added... more like "complexity without much use" :)
# <Discipulus> sweval: my @arr=(0..3); exists $arr[0]
# <perlbot> Discipulus: 1
# <Discipulus> sweval: my @arr=(0..3); exists $arr[42]
# <perlbot> Discipulus: No output.
# <Discipulus> what bad?
# <purl> bad is DCC SEND "LOLDICKS" 0 0 0 or badminton
# <hobbs> I think someone had a dream of removing the distinction entirely
# <hobbs> sweval: my @arr; $arr[1000]=1; exists $arr[0]
# <Discipulus> can someone rephrase that warning, in a bricklayer style? What there is bad in exists $arr[42] ?
# <perlbot> hobbs: No output.
# <hobbs> No. :)
# <Discipulus> ah aha
# <LeoNerd> It's bad because it exposes bits of internal mechanism that aremn't otherwise visible from perl code...
# <LeoNerd> The result depends on the recent history of the array in ways that you can't detect any other way
# <LeoNerd> Specifically, all the slots from 0 up to AvLEN will "exist"
# <LeoNerd> AvLEN will be /at least/ as big as $#array, but in some cases can be larger
# <Discipulus> but when straversing a datastructure, so no modification ar possible, nothing bad should happen, no?
# <LeoNerd> Reading from an array won't change AvLEN no
# <Discipulus> or I would expect perl to die on exists $arr[$i]
# <LeoNerd> But writing new elements into it might.
# <LeoNerd> N it won't *die*, it just might give you weird numbers
# <LeoNerd> It's easily possible to end up with an array where  scalar(@array) is say, 4, but $array[10] still "exists"
# <Discipulus> perhaps the doc needs to explain it better :)
# <LeoNerd> You can end up with phantom elements appearing over the top
# <LeoNerd> No. The doc should just say "don't use this, it is stupid"
# <LeoNerd> It does not, has never, will never give you a /useful/ answer
# <LeoNerd> There is no point fixing it. Just don't use it
# <Discipulus> probably I'm totally off road.. but I interactly receive some $input at nth level of a complex datastructure..
# <Discipulus> what bad if I: if ( exists $data->[0]->{a}->[0]->[$input] ) { ARRAY..
# <LeoNerd> False positives
# <purl> False positives is better than no positives. or https://en.wikipedia.org/wiki/Type_I_and_type_II_errors#Type_I_error
# <LeoNerd> You might be told "yes it exists" when it doesn't
# <LeoNerd> As I said, it is possible to make an array for which scalar(@array) is some number (e.g. 4) but yet  exists $array[10]  claims to be true
# <Discipulus> ah! even just accesing it?
# <LeoNerd> _yes_
# <hobbs> it just doesn't tell you anything that helps you in your life
# <Discipulus> ok this is a definitive argument
# <LeoNerd> As I and the doc keep trying to say: Just stop using `exists` on arrays
# <LeoNerd> Pretend that it is a syntax error
# <LeoNerd> It ought never have been allowed. but it was. so now we can't get rid of it because stupid reasons
# <Discipulus> ok thanks for the patience!
# <hobbs> btw, LeoNerd, it's not *only* about AvFILL, it also gives you false for slots holding (SV*)NULL (which is what my example actually illustrated). And that's what 'delete' creates (or you can just sparsely populate an array, any space that has to be created will be full of nulls, not undef)
# <LeoNerd> Ahyes that too
# <LeoNerd> You can make holes in the middle
# <LeoNerd> $ perl -E 'my @arr = (1); $arr[5] = 4; say "exists [$_]: ", exists $arr[$_] for 0 .. 5';
# <Discipulus> uh nice!
# <shmem> but... but... when is exists $ary[$i] different from defined $ary[$i] ? example?
# <LeoNerd> Try   my @arr = (1, undef, undef, undef, 5);  instead
# <hobbs> when you take LeoNerd's example and add 'undef $arr[2];' to it
# <hobbs> now you have (both, neither, exists but not defined, neither, both)
# <LeoNerd> This is because arrays (and only arrays) can contain _two_ kinds of undef at the C pointer level. They can be a regular PL_sv_undef, or they can be a true C NULL
# <shmem> hm. okay.
# <LeoNerd> _everything_ else in Perl treats PL_sv_undef vs NULL as both being the "undefined" value
# <LeoNerd> exists uniquely can distinguish the two
# <LeoNerd> A slot with PL_sv_undef still "exists", but one with NULL does not
# <Discipulus> ah! this is what I'd call a bricklayer definition
# <shmem> Discipulus: now you can make the windows in your house.
# <shmem> ...in that array of blocks ;-)
# <hobbs> I might actually consider using delete on an array if I came up with something where a sparse array was the most efficient structure, and I was removing things from it, and I was memory-conscious enough to be concerned about 24 bytes per entry
sub open_element{
	my $self = shift;
	print "current element is ",(ref $self->{current})," and contains ",
		#( ref $self->{current} ? $self->{current} : (ref 'ARRAY' eq ref $self->{current} ? scalar @{$self->{current}} : scalar keys %{$self->{current}}) ),
		( ref $self->{current} ? 
			(	
				ref 'ARRAY' eq ref $self->{current} ? 
				scalar @{$self->{current}} 			: 
				scalar keys %{$self->{current}}
			) : $self->{current}
		),
		" subelements\n";
	
	# populate currentlevel
	if ( ref $self->{current} eq 'ARRAY' ){
		$self->{currentlevel} = ( [0 .. $#{$self->{current}} ] );
	}
	elsif( ref $self->{current} eq 'HASH' ){
		$self->{currentlevel} = ( [ sort keys %{$self->{current}} ] );
	}
	else{ $self->{currentlevel} = (  $self->{current}  ) }
	
}


sub _check_opts{
	my %opts = @_;
	
	# COMMANDS
	
	# default dumper
	unless ( defined $opts{commands}{dumper} ){
		require Data::Dumper;
		# Undefined subroutine &Data::Interactive::Walker::Dumper called at lib/Data/Interactive/Walker.pm line 26.
		# $opts{dumper} = sub{ print +Dumper(\$_[0]) }
		# $opts{dumper} = sub{ print Data::Dumper::Dumper\$_[0] }
		# + () to avoid Name "Data::Dumper::Dumper" used only once: possible typo at
		# full qualified name to avoid runtime error
		########$opts{commands}{dumper} = sub{ print +Data::Dumper::Dumper(\$_[0]) }
		$opts{commands}{dumper} = sub{ print +Data::Dumper::Dumper(\$_[0]) }
	}
	
	# FORMATTERS
	foreach my $form ( qw(OTHER OBJECT SCALAR ARRAY HASH CODE REF GLOB LVAVLUE FORMAT IO VSTRING Regexp ) ){
		
		if ( defined $opts{formatters}{$form} ){
			croak "Formatter [$form] needs to contain a coderef!"
				unless 'CODE' eq ref $opts{formatters}{$form};
		}
	}
	
	# unless ( defined $opts{formatters}{ARRAY} ){
		# $opts{formatters}{ARRAY} = sub { my $ref = shift; print +(join, "\n", @$ref),"\n" };
	# }
	
	
	
	return %opts;
}











1; # End of Data::Interactive::Walker


__DATA__

=head1 NAME

Data::Interactive::Walker - The great new Data::Interactive::Walker!

=head1 VERSION

Version 0.01


=head1 SYNOPSIS

Quick summary of what the module does.

Perhaps a little code snippet.

    use Data::Interactive::Walker;

    my $foo = Data::Interactive::Walker->new();
    ...



=head1 AUTHOR

LORENZO, C<< <LORENZO at cpan.org> >>

=head1 BUGS

Please report any bugs or feature requests to C<bug-data-interactive-walker at rt.cpan.org>, or through
the web interface at L<https://rt.cpan.org/NoAuth/ReportBug.html?Queue=Data-Interactive-Walker>.  I will be notified, and then you'll
automatically be notified of progress on your bug as I make changes.




=head1 SUPPORT

You can find documentation for this module with the perldoc command.

    perldoc Data::Interactive::Walker


You can also look for information at:

=over 4

=item * RT: CPAN's request tracker (report bugs here)

L<https://rt.cpan.org/NoAuth/Bugs.html?Dist=Data-Interactive-Walker>

=item * AnnoCPAN: Annotated CPAN documentation

L<http://annocpan.org/dist/Data-Interactive-Walker>

=item * CPAN Ratings

L<https://cpanratings.perl.org/d/Data-Interactive-Walker>

=item * Search CPAN

L<https://metacpan.org/release/Data-Interactive-Walker>

=back


=head1 ACKNOWLEDGEMENTS


=head1 LICENSE AND COPYRIGHT

Copyright 2021 LORENZO.

This program is free software; you can redistribute it and/or modify it
under the terms of either: the GNU General Public License as published
by the Free Software Foundation; or the Artistic License.

See L<http://dev.perl.org/licenses/> for more information.


=cut