#!perl -T
use 5.006;
use strict;
use warnings;
use Test::More;

plan tests => 1;

BEGIN {
    use_ok( 'Data::Interactive::Walker' ) || print "Bail out!\n";
}

diag( "Testing Data::Interactive::Walker $Data::Interactive::Walker::VERSION, Perl $], $^X" );
